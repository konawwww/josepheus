class Josepheus
  def self.kill(total: 40, each: 3)
    soldiers = (1..total).to_a.map { |v| { number: v } }

    index = 0
    each -= 1

    until soldiers.count == 1
      index = ((index + each) % (soldiers.count))
      soldiers.slice!(index)
    end

    soldiers[0][:number]
  end
end
