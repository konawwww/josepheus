require './lib/josepheus'

RSpec.describe Josepheus do
  describe ".kill" do
    context "default (every 3, total 40)" do
      it "returns 28" do
        expect(Josepheus.kill).to eq 28
      end
    end

    context "every 3, total 500" do
      it "returns 436" do
        expect(Josepheus.kill(each: 3, total: 500)).to eq 436
      end
    end
  end
end
